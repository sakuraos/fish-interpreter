/**
 * @file main.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-28
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <deque>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

int base64_decode(std::string str) {
    const std::string lut = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnop"
                            "qrstuvwxyz0123456789+/";
    int               val = lut.find(str[0]) * (2 << 6);
    val += lut.find(str[1]);
    return val;
}

struct ip {
    int row;
    int col;
    enum class dir { up, down, left, right } dir;
};

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "No file provided" << std::endl;
        return 1;
    }
    std::ifstream file;
    file.open(argv[1]);

    if (!file.is_open()) {
        std::cerr << "Invalid file" << std::endl;
        return 1;
    }

    std::string header;
    std::getline(file, header);
    // std::cout << header << std::endl;

    if (header.find("FSH ") != 0) {
        std::cerr << "File header invalid" << std::endl;
        return 1;
    }

    int cols = base64_decode(header.substr(4, 2));
    int rows = base64_decode(header.substr(6, 2));
    // std::cout << "rows: " << rows << " cols: " << cols << std::endl;

    std::vector<std::string> instructions;
    for (int i = 0; i < rows; i++) {
        std::string str;
        std::getline(file, str);
        instructions.push_back(str);
    }

    int running = true;

    int parse = false;

    ip ptr = {0, 0, ip::dir::right};

    std::deque<int> stack;

    std::random_device rd; // Will be used to obtain a seed for the
                           // random number engine
    std::mt19937 gen(
        rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(0, 3);

    while (running != 0) {
        if (parse) {
            if ((instructions[ptr.row][ptr.col] == '\'' &&
                 parse == '\'') ||
                (instructions[ptr.row][ptr.col] == '\"' &&
                 parse == '\"')) {
                parse = false;
            } else {
                stack.push_front(instructions[ptr.row][ptr.col]);
            }
        } else {
            switch (instructions[ptr.row][ptr.col]) {
            case '\"':
                parse = '\"';
                break;
            case '\'':
                parse = '\'';
                break;

            case '<':
                ptr.dir = ip::dir::left;
                break;
            case '>':
                ptr.dir = ip::dir::right;
                break;
            case '^':
                ptr.dir = ip::dir::up;
                break;
            case 'v':
                ptr.dir = ip::dir::down;
                break;

            case '/':
                switch (ptr.dir) {
                case ip::dir::up:
                    ptr.dir = ip::dir::right;
                    break;
                case ip::dir::down:
                    ptr.dir = ip::dir::left;
                    break;
                case ip::dir::left:
                    ptr.dir = ip::dir::down;
                    break;
                case ip::dir::right:
                    ptr.dir = ip::dir::up;
                    break;
                default:
                    break;
                }
                break;
            case '\\':
                switch (ptr.dir) {
                case ip::dir::up:
                    ptr.dir = ip::dir::left;
                    break;
                case ip::dir::down:
                    ptr.dir = ip::dir::right;
                    break;
                case ip::dir::left:
                    ptr.dir = ip::dir::up;
                    break;
                case ip::dir::right:
                    ptr.dir = ip::dir::down;
                    break;
                default:
                    break;
                }
                break;
            case '|':
                switch (ptr.dir) {
                case ip::dir::up:
                    break;
                case ip::dir::down:
                    break;
                case ip::dir::left:
                    ptr.dir = ip::dir::right;
                    break;
                case ip::dir::right:
                    ptr.dir = ip::dir::left;
                    break;
                default:
                    break;
                }
                break;
            case '_':
                switch (ptr.dir) {
                case ip::dir::up:
                    ptr.dir = ip::dir::down;
                    break;
                case ip::dir::down:
                    ptr.dir = ip::dir::up;
                    break;
                case ip::dir::left:
                    break;
                case ip::dir::right:
                    break;
                default:
                    break;
                }
                break;
            case '#':
                switch (ptr.dir) {
                case ip::dir::up:
                    ptr.dir = ip::dir::down;
                    break;
                case ip::dir::down:
                    ptr.dir = ip::dir::up;
                    break;
                case ip::dir::left:
                    ptr.dir = ip::dir::right;
                    break;
                case ip::dir::right:
                    ptr.dir = ip::dir::left;
                    break;
                default:
                    break;
                }
                break;

            case 'x':
                switch (distrib(gen)) {
                case 0:
                    ptr.dir = ip::dir::up;
                    break;
                case 1:
                    ptr.dir = ip::dir::down;
                    break;
                case 2:
                    ptr.dir = ip::dir::left;
                    break;
                case 3:
                    ptr.dir = ip::dir::right;
                    break;
                }
                break;

            case '?':
                if (stack.front() != 0) {
                    stack.pop_front();
                    break;
                } else {
                    stack.pop_front();
                    [[fallthrough]];
                }
            case '!':
                switch (ptr.dir) {
                case ip::dir::up:
                    if (ptr.row == 0) {
                        ptr.row = rows - 1;
                    } else {
                        ptr.row--;
                    }
                    break;
                case ip::dir::down:
                    ptr.row = (ptr.row + 1) % rows;
                    break;
                case ip::dir::left:
                    if (ptr.col == 0) {
                        ptr.col = cols - 1;
                    } else {
                        ptr.col--;
                    }
                    break;
                case ip::dir::right:
                    ptr.col = (ptr.col + 1) % cols;
                    break;

                default:
                    break;
                }
                break;

            case '.': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                ptr.col = x;
                ptr.row = y;
            } break;

            case '+': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y + x);
            } break;
            case '-': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y - x);
            } break;
            case '*': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y * x);
            } break;
            case ',': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y / x);
            } break;
            case '%': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y % x);
            } break;

            case '=': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y == x ? 1 : 0);
            } break;

            case ')': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y > x ? 1 : 0);
            } break;
            case '(': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(y < x ? 1 : 0);
            } break;

            case ':':
                stack.push_front(stack.front());
                break;

            case '~':
                stack.pop_front();
                break;

            case '$': {
                int first = stack.front();
                stack.pop_front();
                int second = stack.front();
                stack.pop_front();
                stack.push_front(first);
                stack.push_front(second);
            } break;

            case '@': {
                int first = stack.front();
                stack.pop_front();
                int second = stack.front();
                stack.pop_front();
                int third = stack.front();
                stack.pop_front();
                stack.push_front(first);
                stack.push_front(third);
                stack.push_front(second);
            } break;

            case '{': {
                int first = stack.front();
                stack.pop_front();
                stack.push_back(first);
            } break;

            case '}': {
                int last = stack.back();
                stack.pop_back();
                stack.push_front(last);
            } break;

            case 'r': {
                std::deque<int> old = stack;
                stack.clear();
                while (!old.empty()) {
                    stack.push_front(old.front());
                    old.pop_front();
                }
            } break;

            case 'l':
                stack.push_front(stack.size());
                break;

            case 'o':
                std::cout << (char)stack.front();
                stack.pop_front();
                break;
            case 'n':
                std::cout << stack.front();
                stack.pop_front();
                break;
            case 'i':
                break;

            case '&':
                break;

            case 'g': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                stack.push_front(instructions[y][x]);
            } break;

            case 'p': {
                int y = stack.front();
                stack.pop_front();
                int x = stack.front();
                stack.pop_front();
                int v = stack.front();
                stack.pop_front();
                instructions[y][x] = v;
            } break;

            case ';':
                running = false;
                break;

            default:
                break;
            }
        }
        switch (ptr.dir) {
        case ip::dir::up:
            if (ptr.row == 0) {
                ptr.row = rows - 1;
            } else {
                ptr.row--;
            }
            break;
        case ip::dir::down:
            ptr.row = (ptr.row + 1) % rows;
            break;
        case ip::dir::left:
            if (ptr.col == 0) {
                ptr.col = cols - 1;
            } else {
                ptr.col--;
            }
            break;
        case ip::dir::right:
            ptr.col = (ptr.col + 1) % cols;
            break;

        default:
            break;
        }
    }
}
